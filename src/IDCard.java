
public class IDCard extends Card {
	private String idNumber;
	
	public IDCard(String n, String num) {
		super(n);
		this.idNumber = num;
	}
	
	 public String format()
	 {
		 return super.format() + ", ID: " + idNumber;
	 }
	
}
