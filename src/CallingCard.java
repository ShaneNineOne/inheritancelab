
public class CallingCard extends Card {
	private String cardNumber;
	private String pin;
	
	public CallingCard(String n, String num, String newPin) {
		super(n);
		this.cardNumber = num;
		this.pin = newPin;
	}
	
	public String format() {
		return super.format() + ", Card Number: " + cardNumber + ", PIN: " + pin;
	}
	
	
}
