import java.time.Year;

public class DriverLicense extends Card {
	private String expirationYear;
	
	public DriverLicense(String n, String expYear) {
		super(n);
		this.expirationYear = expYear;
	}
	
	public String format() {
		return super.format() + ", Expiration Year: " + expirationYear; 
	}
	
	public boolean isExpired() {
		return Integer.parseInt(this.expirationYear) < Year.now().getValue();
	}
}
