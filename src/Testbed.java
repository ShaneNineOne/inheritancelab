
public class Testbed {

	public static void main(String[] args) {
		IDCard testId = new IDCard("Shane", "2184");
		CallingCard testCalling = new CallingCard("Shane", "2480", "0129");
		DriverLicense testLicense = new DriverLicense("Shane", "2016");
		DriverLicense testLicense2 = new DriverLicense("Shane", "2017");
		DriverLicense testLicense3 = new DriverLicense("Shane", "2018");
		
		System.out.println(testId.format());
		System.out.println(testCalling.format());
		System.out.println(testLicense.format());
		
		System.out.println(testLicense.isExpired());
		System.out.println(testLicense2.isExpired());
		System.out.println(testLicense3.isExpired());
		
	}

}
